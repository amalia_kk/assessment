## Assessment



### Creating the machines


#### Ubuntu

- Once logged in, click 'Launch instances'
- Select Ubuntu Server 18.04
- Click 'Next: Configure Instance Details' 
- Click 'Next: Add Storage'
- Click 'Next: Add Tags'
- Click 'Add Tag' 
- In the Key section, type: name 
- In the Value section, type: yourfirstname-ubuntu
- Add another tag and in the Key section, type: Project
- In the value section, type: assessment1
- Click 'Next: Configure Security Group'
- Create a new security group and alter the first that is already there. Under Source, select 'My IP' from the dropdown box. Then in the Description, type: Allow from my IP
- Add another rule and select HTTP from the dropdown box under Type. Under Source, select Anywhere and in thr Description, type: Allow from anywhere
- Click Review and Launch
- Create a new key pair or select an existing pair if you already have one
- Click 'Launch Instances'
  

Now the same has to be done for the AWS Linux machine with some slight changes:

- Select Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
- The Description for the first tag will be yourfirstname-aws
- Select an existing Security Group (the one created above)
- The rest is the same


Finally, for the third machine:

- Carry out the same steps as the Ubuntu machine


Apologies if the scripts have errors or can't run, I didn't have time to check them.